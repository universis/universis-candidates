# @universis/candidates

Universis api server plugin for study program candidates, internship selection etc

## Installation

npm i @universis/candidates

## Usage

Register `CandidateService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/candidates#CandidateService"
        }
    ]

Add `CandidateSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/candidates#CandidateSchemaLoader"
                    }
                ]
            }
        }
    }

## Create candidate user

`AfterCreateCandidateUser` service allows universis api to automatically create an OAuth2 server user for any candidate.

If you want to include this operation while using `CandidateStudent.createUser()` method add one more service in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/candidates#AfterCreateCandidateUser"
        }
    ]
    
## Create student

`CreateStudentAfterAcceptCandidate` service allows universis api to automatically create a student after accepting study program request.

If you want to include this operation while accepting StudyProgramRegisterAction add one more service in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/candidates#CreateStudentAfterAcceptCandidate"
        }
    ]

## Send sms notification to a candidate

`@universis/candidates#SendSmsAfterCreateCandidateUser` service sends a sms message to a candidate by using `CandidateStudent.person.mobilePhone` `@universis/candidates` includes a default template for this operation:

```
VISIT https://students.universis.io/register/ USE YOUR INSCRIPTION NUMBER AND THIS ACTIVATION CODE: 00000000000 TO COMPLETE YOUR REGISTRATION
```

This template should be inserted in `MailConfigurations`:

```
{
    "target": "NotifyCandidateUserAction",
    "status": 3,
    "subject": "Admissions - Activate account",
    "template": "send-sms",
    "state": 2
}
```

Of course this operation is configurable by creating another template and insert it in `MailConfigurations`.

Finally `@universis/candidates#SendSmsAfterCreateCandidateUser` should be added in application services collection:

```
{
      "serviceType": "@universis/candidates#SendSmsAfterCreateCandidateUser"
},
{
    "serviceType": "@universis/messages#SmsService",
    "strategyType": "..."
}
```
Important note: `@universis/messages#SmsService` strategy should be registered also in order to enable sending sms messages.

A notification like that always wants to inform user about an application in order to give him access to this application by providing a url. The template sms message uses `WebApplication` model in order to get information about register app. A record should be inserted to point to universis-register application.

```
{
    "alternateName": "register",
    "softwareVersion": "latest",
    "applicationSuite": "universis",
    "screenshot": "https://gitlab.com/universis/universis-register/-/raw/main/universis-register-dashboard.png",
    "name": "Admissions",
    "url": "https://students.universis.io/register/"
}
```

A user who is going to send sms message should have `CandidateStudent/SendActivationMessage` privilege e.g. 

```
{
    "privilege": "CandidateStudent/SendActivationMessage",
    "parentPrivilege": null,
    "workspace": 1,
    "account": {
        "name": "Administrators"
    },
    "target": "0",
    "mask": 16
}
```



