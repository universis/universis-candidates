import { DataObject, EdmMapping, EdmType } from "@themost/data";
import { HttpForbiddenError } from "@themost/common";

class StudyProgramEnrollmentEvent extends DataObject {
    constructor() {
        super();
    }

    @EdmMapping.func('showRequests', EdmType.CollectionOf('StudyProgramRegisterAction'))
    async getStudyProgramRegisterActions() {
        const enrollmentEvent = await this.context
            .model('StudyProgramEnrollmentEvent')
            .where('viewers/name')
            .equal(this.context.user.name)
            .and("id")
            .equal(this.getId())
            .select('id')
            .getItem();

        if (enrollmentEvent) {
            return this.context.model('StudyProgramRegisterAction')
                .where('studyProgramEnrollmentEvent')
                .equal(enrollmentEvent.id)
                .and('actionStatus/alternateName')
                .notEqual('PotentialActionStatus')
                .select('requests')
                .expand(
                    {
                        'name': 'reviews',
                        'options': {
                            '$expand': 'createdBy($select=UserInfo)'
                        },
                    },
                    {
                        'name': 'attachments'
                    },
                    {
                        'name': 'messages',
                        'options': {
                            '$expand': 'attachments,sender($select=id,name,alternateName)',
                            '$orderby': 'dateCreated desc'
                        }
                    },
                    {
                        'name': 'specialization',
                        'options': {
                            '$select': 'id, name, specialty'
                        }
                    }
                )
                .silent()
                .prepare();
        } else {
            throw new HttpForbiddenError("User is not a viewer of the given enrollment event");
        }
    }
}

module.exports = StudyProgramEnrollmentEvent;
