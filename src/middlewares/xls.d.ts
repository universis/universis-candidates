import {RequestHandler} from "express";

declare interface PostXlsOptions {
    name: string;
}

declare interface PostXlsWithConfigOptions extends PostXlsOptions{
    schema: Object;
}

export declare function xlsPostParserWithConfig(options?: PostXlsOptions): RequestHandler;
