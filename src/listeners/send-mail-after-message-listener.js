import { getMailer } from '@themost/mailer';
import {TraceUtils} from "@themost/common";
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 1) {
        const context = event.model.context;
        const recipient = await context.model('Account').find(event.target.recipient).silent().getItem();
        if (recipient == null) {
            return;
        }
        // get mail template
        const mailTemplate = await context.model('MailConfiguration').where('target').equal('CandidateSendEmailAfterMessage').silent().getItem();
        if (mailTemplate == null) {
            return;
        }
        /**
         * @type {import('@themost/mailer').MailerHelper}
         */
        const mailer = getMailer(context);
        // get parent action data
        const item = await context.model('StudyProgramRegisterAction')
            .where('id').equal(event.target.action)
            .expand({
                name: 'candidate',
                options: {
                    $expand: 'person'
                }
            }, 'owner')
            .silent().getItem();
        if (item) {
            if (recipient.id !== item.owner.id) {
                return;
            }
            // check if candidate's email exists
            const email = item.candidate.person.email;
            if (email== null || typeof email !== 'string') {
                TraceUtils.warn(`Cannot send email for ${item.candidate.person.familyName} ${item.candidate.person.givenName}. Candidate [${item.candidate.inscriptionNumber}] email does not exist.`);
                return;
            }
            await new Promise((resolve) => {
                mailer.template(mailTemplate.template)
                    .subject(mailTemplate.subject)
                    .bcc(mailTemplate.bcc || '')
                    .to(email)
                    .send({
                        model: item
                    }, (err) => {
                        if (err) {
                            try {
                                TraceUtils.error('An error occurred while trying to send an email notification after candidate message.');
                                TraceUtils.error(`candidate=${item.candidate.id}, template=${mailTemplate.template}`);
                                TraceUtils.error(err);
                            } catch (err1) {
                                // do nothing
                            }
                            return resolve();
                        }
                        return resolve();
                    })
            });
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
