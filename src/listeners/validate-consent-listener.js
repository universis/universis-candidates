import { DataError, DataNotFoundError } from '@themost/common';
import {DataObjectState} from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    // validate consent if current user is the owner of this object (candidate)
    let owner = event.target.owner;
    const context = event.model.context;
    let agree = false;
    if (owner) {
        // get owner
        owner = await context.model('User').find(event.target.owner).select('name').silent().value();
    } else {
        if (event.state === DataObjectState.Update) {
            owner = await context.model('User').find(event.previous.owner).select('name').silent().value();
        } else { // throw error
            throw new DataNotFoundError('Action owner cannot be found or is inaccessible', null, event.model.name, event.target.id);
        }
    }
    if (owner === context.user.name) {
        if (Object.prototype.hasOwnProperty.call(event.target, 'agree')) {
            agree = event.target.agree;
        } else {
            if (event.state === DataObjectState.Update) {
                agree = await context.model(event.model.name).where('id').equal(event.target.id).select('agree').value();
            }
        }
        if (!agree) {
            throw Object.assign(new DataError('ERR_CONSENT_REQUIRED', 'If consent is not provided, the application cannot be submitted', null, 'StudyProgramRegisterAction'), {
                statusCode: 409
            });
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
