import { DataError, DataNotFoundError } from '@themost/common';


function validateEnrollmentPeriod(enrollmentPeriod) {
    const currentDate = new Date().setHours(0, 0, 0, 0);
    let valid = false;
    if (enrollmentPeriod.validFrom instanceof Date) {
        const validFrom = enrollmentPeriod.validFrom.setHours(0, 0, 0, 0);
        if (enrollmentPeriod.validThrough instanceof Date) {
            const validThrough = enrollmentPeriod.validThrough.setHours(0, 0, 0, 0);
            valid = validFrom <= currentDate && validThrough >= currentDate;
        } else {
            valid = validFrom <= currentDate;
        }
    } else if (enrollmentPeriod.validThrough instanceof Date) {
        const validThrough = enrollmentPeriod.validThrough.setHours(0, 0, 0, 0);
        valid = validThrough >= currentDate;
    }
    return valid;
}
/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    // check if current user is the owner of this object
    let owner;
    let enrollmentPeriod;
    const context = event.model.context;
    if (event.state === 2) {
        if (Object.prototype.hasOwnProperty.call(event.target, 'owner')) {
            // get owner
            owner = await context.model('User').find(event.target.owner).select('name').silent().value();
        } else {
            owner = await context.model('User').find(event.previous.owner).select('name').silent().value();
        }
        // get enrollment event
        const item = await context.model('StudyProgramRegisterAction')
            .where('id').equal(event.target.id)
            .select('id', 'studyProgramEnrollmentEvent')
            .expand('studyProgramEnrollmentEvent')
            .silent()
            .getItem();
        enrollmentPeriod = item.studyProgramEnrollmentEvent;
    } else if (event.state === 1) {
        // get owner
        owner = await context.model('User').find(event.target.owner).select('name').silent().value();
        const inscriptionMode = event.target.inscriptionMode && (event.target.inscriptionMode.id || event.target.inscriptionMode);
        // on insert, if the enrollment event is provided in target payload
        if (event.target.studyProgramEnrollmentEvent) {
            const targetEnrollmentEvent = event.target.studyProgramEnrollmentEvent.id || event.target.studyProgramEnrollmentEvent;
            // validate event against studyProgram, year, period, mode and eventStatus
            enrollmentPeriod = await context.model('StudyProgramEnrollmentEvent')
                .where('id').equal(targetEnrollmentEvent)
                .and('studyProgram').equal(event.target.studyProgram)
                .and('inscriptionYear').equal(event.target.inscriptionYear)
                .and('inscriptionPeriod').equal(event.target.inscriptionPeriod)
                .and('eventStatus/alternateName').equal('EventOpened')
                .expand('inscriptionModes')
                .silent()
                .getItem();
            // only if inscriptionModes are defined
            // and the candidate's inscription mode is not included
            if (enrollmentPeriod
                && Array.isArray(enrollmentPeriod.inscriptionModes)
                && enrollmentPeriod.inscriptionModes.length > 0
                && !enrollmentPeriod.inscriptionModes.find((item) => item.id === inscriptionMode)) {
                    // nullify enrollmentPeriod (not valid)
                    enrollmentPeriod = null;
            }
        } else {
            // auto-assign event
            // fetch open events, for the specified studyProgram, year and period
            const enrollmentPeriods = await context.model('StudyProgramEnrollmentEvent')
                .where('studyProgram').equal(event.target.studyProgram)
                .and('inscriptionYear').equal(event.target.inscriptionYear)
                .and('inscriptionPeriod').equal(event.target.inscriptionPeriod)
                .and('eventStatus/alternateName').equal('EventOpened')
                .expand('inscriptionModes')
                .orderByDescending('id')
                .silent()
                .getAllItems();
            enrollmentPeriod = Array.isArray(enrollmentPeriods) ?
                enrollmentPeriods.find((enrollmentEvent) => {
                    // assign the first event that either has empty inscription modes
                    // or includes the candidate's inscription mode
                    return Array.isArray(enrollmentEvent.inscriptionModes) &&
                        (enrollmentEvent.inscriptionModes.length === 0
                        || enrollmentEvent.inscriptionModes.find((item) => item.id === inscriptionMode));
                })
            : null;
        }
        if (enrollmentPeriod) {
            // set enrollment event
            event.target.studyProgramEnrollmentEvent = enrollmentPeriod.id;
        } else {
            throw Object.assign(new DataError('ERR_ENROLLMENT_EVENT', 'There is no StudyProgramEnrollmentEvent that the candidate is elligible for.', null, 'StudyProgramEnrollmentEvent'), {
                statusCode: 409
            });
        }
    }
    if (owner == null) {
        throw new DataNotFoundError('Action owner cannot be found or is inaccessible', null, event.model.name, event.target.id);
    }
    if (owner === context.user.name) {
        // validate period
        if (enrollmentPeriod == null) {
            throw Object.assign(new DataError('ERR_MISSING_PERIOD', 'The enrollment period of the specified study program cannot be found', null, 'StudyProgramEnrollmentEvent'), {
                statusCode: 409
            });
        }
        const valid = validateEnrollmentPeriod(enrollmentPeriod);
        if (valid === false) {
            throw Object.assign(new DataError('ERR_EXPIRED_PERIOD', 'The enrollment period has expired', null, 'StudyProgramEnrollmentEvent'), {
                statusCode: 409
            });
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
