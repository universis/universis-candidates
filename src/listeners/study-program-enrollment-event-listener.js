import { DataObjectState } from '@themost/data';
import { promisify } from 'util';
import { DataPermissionEventListener, PermissionMask } from '@themost/data';
import { HttpForbiddenError } from '@themost/common';

/**
 * @param {DataEventArgs} event
 */
async function beforeRemoveAsync(event) {
    const context = event.model.context;
    // remove also attachment types
    const attachmentTypes = await context.model('EnrollmentAttachmentConfiguration').where('object').equal(event.target.identifier).silent().getItems();
    await context.model('EnrollmentAttachmentConfiguration').remove(attachmentTypes);
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    // operate only if target object has the autoRegisterCourses property
    if (!event.target.hasOwnProperty('autoRegisterCourses')) {
        return;
    }
    // get previous and target states
    const previousAutoRegisterValue = event.state === DataObjectState.Insert
        ? null : event.previous && event.previous.autoRegisterCourses;
    const targetAutoRegisterValue = event.target.autoRegisterCourses;
    // exit if this is an update operation and the value has not changed
    // or if this is an insert operation and the value is null or false
    if ((event.state === DataObjectState.Update && previousAutoRegisterValue === targetAutoRegisterValue)
        || (event.state === DataObjectState.Insert && !targetAutoRegisterValue)) {
        return;
    }
    // validate StudyProgramEnrollmentEvent/ModifyAutoCourseRegistration execute permission
    const canModifyAutoRegisterValue = {
        model: event.model,
        privilege: `${event.model.name}/ModifyAutoCourseRegistration`,
        mask: PermissionMask.Execute,
        target: event.target,
        throwError: false
    };
    const validator = new DataPermissionEventListener();
    const validateAsync = promisify(validator.validate)
    await validateAsync(canModifyAutoRegisterValue);
    if (canModifyAutoRegisterValue.result === true) {
        return;
    }
    // throw forbidden error
    throw new HttpForbiddenError();
}
