import express from 'express';
import { multerInstance } from './middlewares/multer';
import { TraceUtils } from '@themost/common';
import path from 'path';


function studyProgramEnrollmentEventRouter(configuration) {
    const router = express.Router();
    const upload = multerInstance(configuration);
    upload.storage.getDestination(null, null, (err, destination) => {
        if (err) {
            TraceUtils.error(`Routes: An error occurred while initializing instructors router.`);
            return TraceUtils.error(err);
        }
        TraceUtils.info(`Routes: Instructors router starts using "${path.resolve(destination)}" as user storage.`);
    });

    const validateUser = async (context, enrollmentEventId) => {
        const enrollmentEvent = await context
            .model('StudyProgramEnrollmentEvent')
            .where('viewers/name')
            .equal(context.user.name)
            .and("id")
            .equal(enrollmentEventId)
            .select('id')
            .value();

        if (enrollmentEvent) {
            return true;
        } 
        return false;
    }

    router.post('/:id/requests/:request/review', async function getEntity(req, res, next) {
        try {
            const request = await req.context.model('StudyProgramRegisterAction').where('id').equal(req.params.request).silent().getTypedItem();
            await request.setReview(req.body)
            return res.json();
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/requests/:request/messages', async function getEntity(req, res, next) {
        try {
            const context = req.context;
            const enrollmentEvent = req.params.id;
            const request = req.params.request;

            let query = context
                .model('StudyProgramRegisterActionMessage')
                .where('action')
                .equal(request)
                .expand(
                    {
                        name: 'attachments'
                    },
                    {
                        name: 'sender',
                        options: {
                            $select: 'id,name,alternateName'
                        }
                    }
                )
                .orderByDescending('dateCreated');

            if (await validateUser(context, enrollmentEvent) === true) {
                return res.json(await query.silent().getItems());
            }
            return res.json(await query.getItems());
        } catch (err) {
            return next(err);
        }
    });

    router.post('/:id/requests/:request/message', async function getEntity(req, res, next) {
        try {
            const request = await req.context.model('StudyProgramRegisterAction').where('id').equal(req.params.request).silent().getTypedItem();
            await request.postMessages(req.body)
            return res.json();
        } catch (err) {
            return next(err);
        }
    });


    router.post('/:id/requests/:request/sendMessage', upload.single('attachment'), async function sendCourseClassStudentMessages(req, res, next) {
        try {
            const request = await req.context.model('StudyProgramRegisterAction').where('id').equal(req.params.request).silent().getTypedItem();
            await request.sendMessage(req.file, req.body)
            return res.json();
        }
        catch (err) {
            return next(err);
        }
    });

    return router;
}

export {
    studyProgramEnrollmentEventRouter
}
